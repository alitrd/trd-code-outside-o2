#if !defined(__CLING__) || defined(__ROOTCLING__)

#include <array>
#include <bitset>
#include <chrono>
#include <cmath>
#include <map>
#include <string>
#include <vector>

// ROOT header
#include "TLatex.h"
#include "TLegend.h"
#include "TROOT.h"
#include <TBranch.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TFile.h>
#include <TH2.h>
#include <THStack.h>
#include <THistPainter.h>
#include <TMath.h>
#include <TPad.h>
#include <TPaveStats.h>
#include <TStyle.h>

// O2 header
#include "DataFormatsTRD/NoiseCalibration.h"
#include "GlobalTracking/MatchTOF.h"
#include "ReconstructionDataFormats/PID.h"
#include "ReconstructionDataFormats/TrackLTIntegral.h"
#include "TOFBase/Geo.h"
#include "TRDQC/Tracking.h"
#include <CommonConstants/PhysicsConstants.h>
#include <DataFormatsTPC/BetheBlochAleph.h>
#include <Framework/HistogramSpec.h>
#endif

using namespace o2::track;
using namespace o2::framework;
using namespace o2::constants::physics;

#define PBSTR "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
#define PBWIDTH 100

#define P_MIN 0.5
#define P_MAX 8.0
#define P_BINS 200

#define DE_MIN 20
#define DE_MAX 150
#define DE_BINS 500

#define BETA_MIN 0.0
#define BETA_MAX 1.05
#define BETA_BINS 500

#define TOF_CUT_MAX 100
#define TOF_CUT_MIN 72.5

constexpr double timeAvg{0.12};
const double dEdxResolution{0.08};
constexpr double sigma{3.0};

inline int highBit(uint8_t a)
{
  auto b = a;
  int r{0};
  while (b >>= 1) {
    r++;
  }
  return r;
}

inline void printProgress(
  const std::chrono::time_point<std::chrono::high_resolution_clock>& clock,
  long long entries, double percentage)
{
  int val = (int)(percentage * 100);
  int lpad = (int)(percentage * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
    std::chrono::duration_cast<std::chrono::seconds>(stop - clock);
  printf("\r%3d%% [%.*s%*s] %ld (s) (Est. Remaining %ld (s))", val, lpad, PBSTR,
         rpad, "", duration.count(),
         static_cast<long>((1.0 - percentage) * entries * timeAvg));
  fflush(stdout);
}

// Give mass as par[0]
Double_t BetheBloch(Double_t* x, Double_t* par)
{
  const Double_t p1 = 0.15429793, p2 = 5.255017, p3 = 0.0028807498,
                 p4 = 2.3966658, p5 = 1.2244654, mMIP = 50.;
  Double_t xx = x[0];
  Double_t bg = xx / par[0];
  Double_t f = o2::tpc::BetheBlochAleph(bg, p1, p2, p3, p4, p5) * mMIP;
  return f;
}

// Polygon is graphical cut in TPC dEdx
static constexpr double polygon[4][2] = {
  {1.5, 74.0},
  {9.0, 74.0},
  {9.0, 82.2},
  {1.5, 82.2},
};

// simple Point in Polygon algorithm
bool isTPCCut(double p, double dEdx)
{
  double point[2] = {p, dEdx};
  // A point is in a polygon if a line from the point to infinity crosses the
  // polygon an odd number of times
  bool odd = false;
  // For each edge (In this case for each point of the polygon and the previous
  // one)
  for (int i = 0, j = 4 - 1; i < 4;
       i++) { // Starting with the edge from the last to the first node
    // If a line from the point into infinity crosses this edge
    if (((polygon[i][1] > point[1]) !=
         (polygon[j][1] >
          point[1])) // One point needs to be above, one below our y coordinate
                     // ...and the edge doesn't cross our Y corrdinate before
                     // our x coordinate (but between our x coordinate and
                     // infinity)
        && (point[0] < (polygon[j][0] - polygon[i][0]) *
                           (point[1] - polygon[i][1]) /
                           (polygon[j][1] - polygon[i][1]) +
                         polygon[i][0])) {
      // Invert odd
      odd = !odd;
    }
    j = i;
  }
  // If the number of crossings was odd, the point is in the polygon
  return odd;
}

void particleSkimmer()
{
  gROOT->SetBatch(true);
  gStyle->SetOptStat("e");
  gStyle->SetPalette("batlow.txt");
  //----------------------------------------------------
  // Chain and Branch
  //----------------------------------------------------
  // TRD QC
  TChain chainQC("qc");
  chainQC.Add("trdQC*.root");
  std::vector<o2::trd::TrackQC> qc, *qcPtr{&qc};
  chainQC.SetBranchAddress("trackQC", &qcPtr);

  // TPC-TRD-TOF Info
  TChain chainTPCTRDTOF("matchTOF");
  chainTPCTRDTOF.Add("o2match_tof_tpctrd*.root");
  std::vector<o2::dataformats::MatchInfoTOF> TPCTRDTOFMatchInfo,
    *TPCTRDTOFMatchInfoPtr{&TPCTRDTOFMatchInfo};
  chainTPCTRDTOF.SetBranchAddress("TOFMatchInfo", &TPCTRDTOFMatchInfoPtr);

  // ITS-TPC-TRD-TOF Info
  TChain chainITSTPCTRDTOF("matchTOF");
  chainITSTPCTRDTOF.Add("o2match_tof_itstpctrd*.root");
  std::vector<o2::dataformats::MatchInfoTOF> ITSTPCTRDTOFMatchInfo,
    *ITSTPCTRDTOFMatchInfoPtr{&ITSTPCTRDTOFMatchInfo};
  chainITSTPCTRDTOF.SetBranchAddress("TOFMatchInfo", &ITSTPCTRDTOFMatchInfoPtr);

  // Noisemap
  o2::trd::NoiseStatusMCM noiseMap;
  auto fInNoiseMap = TFile::Open("mcmNoiseMap.root");
  auto noiseMapPtr = (o2::trd::NoiseStatusMCM*)fInNoiseMap->Get("map");
  noiseMap = *noiseMapPtr;
  fInNoiseMap->Close();

  //----------------------------------------------------
  // Histograms
  //----------------------------------------------------
  AxisSpec ptAxisZ{500, 0.48, 8.0};
  ptAxisZ.makeLogarithmic();
  auto dEdxBefore = new TH2D("dEdxTPC_before", "TPC dEdx", ptAxisZ.getNbins(),
                             ptAxisZ.binEdges.data(), 500, 25, 125);
  dEdxBefore->SetTitle("TPC dE/dx");
  dEdxBefore->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxBefore->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxBefore->GetXaxis()->SetTitleOffset(1.25);
  auto dEdxAfter =
    new TH2D("dEdxTPC_After", "TPC dEdx w. Cuts", ptAxisZ.getNbins(),
             ptAxisZ.binEdges.data(), 500, 25, 125);
  dEdxAfter->SetTitle("TPC dE/dx");
  dEdxAfter->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxAfter->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxAfter->GetXaxis()->SetTitleOffset(1.25);

  auto dEdx = new TH2D("dEdx", "dEdx", 2 * P_BINS, -P_MAX, P_MAX, DE_BINS,
                       DE_MIN, DE_MAX);
  dEdx->SetTitle("TPC dE/dx");
  dEdx->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdx->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdx->GetXaxis()->SetTitleOffset(1.25);

  auto dEdxRestTPC = new TH2D("dEdxRestTPC", "dEdx Rest TPC Cut", 2 * P_BINS,
                              -P_MAX, P_MAX, DE_BINS, DE_MIN, DE_MAX);
  dEdxRestTPC->SetTitle("TPC dE/dx - Rest TPC Cut");
  dEdxRestTPC->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxRestTPC->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxRestTPC->GetXaxis()->SetTitleOffset(1.25);

  auto dEdxElectronsTPC =
    new TH2D("dEdxElectronsTPC", "dEdx Electrons TPC Cut", 2 * P_BINS, -P_MAX,
             P_MAX, DE_BINS, DE_MIN, DE_MAX);
  dEdxElectronsTPC->SetTitle("TPC dE/dx - Electrons TPC Cut");
  dEdxElectronsTPC->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxElectronsTPC->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxElectronsTPC->GetXaxis()->SetTitleOffset(1.25);

  auto dEdxRestTOF = new TH2D("dEdxRestTOF", "dEdx Rest TOF Cut", 2 * P_BINS,
                              -P_MAX, P_MAX, DE_BINS, DE_MIN, DE_MAX);
  dEdxRestTOF->SetTitle("TPC dE/dx - Rest TOF Cut");
  dEdxRestTOF->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxRestTOF->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxRestTOF->GetXaxis()->SetTitleOffset(1.25);

  auto dEdxPionsTPC = new TH2D("dEdxPionsTOF", "dEdx Pions", 2 * P_BINS, -P_MAX,
                               P_MAX, DE_BINS, DE_MIN, DE_MAX);
  dEdxPionsTPC->SetTitle("TPC dE/dx - PIons");
  dEdxPionsTPC->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxPionsTPC->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxPionsTPC->GetXaxis()->SetTitleOffset(1.25);

  auto dEdxElectronsTOF =
    new TH2D("dEdxElectronsTOF", "dEdx Electrons TOF Cut", 2 * P_BINS, -P_MAX,
             P_MAX, DE_BINS, DE_MIN, DE_MAX);
  dEdxElectronsTOF->SetTitle("TPC dE/dx - Electrons TOF Cut");
  dEdxElectronsTOF->GetYaxis()->SetTitle("dE/dx (a.u)");
  dEdxElectronsTOF->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  dEdxElectronsTOF->GetXaxis()->SetTitleOffset(1.25);

  auto tof = new TH2D("tof", "TOF", 2 * P_BINS, -P_MAX, P_MAX, BETA_BINS,
                      BETA_MIN, BETA_MAX);
  tof->SetTitle("TOF beta");
  tof->GetYaxis()->SetTitle("#beta");
  tof->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  tof->GetXaxis()->SetTitleOffset(1.25);

  auto tofElectrons = new TH2D("tofElectrons", "TOF Electrons", 2 * P_BINS,
                               -P_MAX, P_MAX, BETA_BINS, BETA_MIN, BETA_MAX);
  tofElectrons->SetTitle("TOF beta - Electrons");
  tofElectrons->GetYaxis()->SetTitle("#beta");
  tofElectrons->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  tofElectrons->GetXaxis()->SetTitleOffset(1.25);

  auto tofRest = new TH2D("tofRest", "TOF Rest", 2 * P_BINS, -P_MAX, P_MAX,
                          BETA_BINS, BETA_MIN, BETA_MAX);
  tofRest->SetTitle("TOF beta - Rest");
  tofRest->GetYaxis()->SetTitle("#beta");
  tofRest->GetXaxis()->SetTitle("#it{p} (GeV/#it{c})");
  tofRest->GetXaxis()->SetTitleOffset(1.25);
  auto tofTPC = new TH2D("tofTPC", "TOF vs. TPC", BETA_BINS, BETA_MIN, BETA_MAX,
                         DE_BINS, DE_MIN, DE_MAX);
  tofTPC->SetTitle("TOF beta vs. TPC dEdx");
  tofTPC->GetXaxis()->SetTitle("#beta");
  tofTPC->GetYaxis()->SetTitle("dE/dx (a.u)");

  auto etaSplit = [] {
    std::vector<TH1D*> vec;
    for (int i = 0; i < 6; ++i) {
      auto h = new TH1D(Form("etaSplit%d", i), Form("L%i", i), 100, -1.f, 1.f);
      h->GetXaxis()->SetTitle("#eta");
      h->GetYaxis()->SetTitle("counts");
      vec.push_back(h);
    }
    return vec;
  }();
  // Functions
  auto fPion = new TF1("fPion", BetheBloch, P_MIN, P_MAX, 1);
  auto fElectron = new TF1("fElectron", BetheBloch, P_MIN, P_MAX, 1);
  auto fMuon = new TF1("fMuon", BetheBloch, P_MIN, P_MAX, 1);
  auto fKaon = new TF1("fKaon", BetheBloch, P_MIN, P_MAX, 1);
  auto fProton = new TF1("fProton", BetheBloch, P_MIN, P_MAX, 1);
  fPion->SetParameter(0, MassPionCharged);
  fPion->SetNpx(1000);
  fElectron->SetParameter(0, MassElectron);
  fElectron->SetNpx(1000);
  fMuon->SetParameter(0, MassMuon);
  fMuon->SetNpx(1000);
  fKaon->SetParameter(0, MassKaonCharged);
  fKaon->SetNpx(1000);
  fProton->SetParameter(0, MassProton);
  fProton->SetNpx(1000);
  auto fEPSep = new TF1(
    "electronProtonSep",
    [&](double* x, double* p) {
      return TMath::Abs(fElectron->Eval(x[0]) - fProton->Eval(x[0])) / (fElectron->Eval(x[0]) * dEdxResolution);
    },
    P_MIN, P_MAX, 0);
  fEPSep->SetNpx(1000);
  auto fEKSep = new TF1(
    "electronKaonSep",
    [&](double* x, double* p) {
      return TMath::Abs(fElectron->Eval(x[0]) - fKaon->Eval(x[0])) / (fElectron->Eval(x[0]) * dEdxResolution);
    },
    P_MIN, P_MAX, 0);
  fEKSep->SetNpx(1000);
  auto fCombSep = new TF1(
    "combinedSep",
    [&](double* x, double* p) {
      auto ep = fEPSep->Eval(x[0]);
      auto ek = fEKSep->Eval(x[0]);
      return std::min(ep, ek);
    },
    P_MIN, P_MAX, 0);
  fCombSep->SetNpx(1000);

  // Out file
  std::unique_ptr<TFile> outFilePtr(
    TFile::Open("particleSkimmer.root", "RECREATE"));

  // CTree
  auto outTree = new TTree("skimmedData", "Tree holding skimmed data");
  int sign;
  outTree->Branch("sign", &sign);
  int pid; // 0=hadron, 1=electron, 2=pion
  outTree->Branch("pid", &pid);
  uint8_t goodTracklets;
  outTree->Branch("goodTracklets", &goodTracklets);
  uint8_t nTracklets;
  outTree->Branch("nTracklets", &nTracklets);
  std::array<int, 6> det;
  outTree->Branch("det", &det);
  std::array<float, 6> pOut;
  outTree->Branch("momentum", &pOut);
  std::array<float, 6> etaOut;
  outTree->Branch("eta", &etaOut);
  std::array<float, 6> phiOut;
  outTree->Branch("phi", &phiOut);
  std::array<int, 6> slope;
  outTree->Branch("slope", &slope);
  std::array<float, 18> charge;
  outTree->Branch("charge", &charge);
  std::array<int, 18> chargeUn;
  outTree->Branch("chargeUn", &chargeUn);
  float dEdxTPC;
  outTree->Branch("dEdx", &dEdxTPC);
  float tofMass;
  outTree->Branch("tofMass", &tofMass);

  //----------------------------------------------------
  // Loop - Vars
  //----------------------------------------------------
  long long nEntries = chainQC.GetEntries();
  if (nEntries != chainTPCTRDTOF.GetEntries() ||
      nEntries != chainITSTPCTRDTOF.GetEntries()) {
    printf("QC: %lld\n", nEntries);
    printf("TPCTRDTOF: %lld\n", chainTPCTRDTOF.GetEntries());
    printf("ITSTPCTRDTOF: %lld\n", chainITSTPCTRDTOF.GetEntries());
    printf("Uneven chains!\n");
    return;
  }
  constexpr float kCSPEED =
    TMath::C() * 1.0e2f * 1.0e-12f; /// Speed of light in TOF units (cm/ps)

  //----------------------------------------------------
  // Loop
  //----------------------------------------------------
  const auto start = std::chrono::high_resolution_clock::now();
  printf("Got a total %lld entries\n", nEntries);
  for (auto iEntry = 0; iEntry < nEntries; ++iEntry) {
    // Load tree entries
    chainQC.GetEntry(iEntry);
    chainTPCTRDTOF.GetEntry(iEntry);
    chainITSTPCTRDTOF.GetEntry(iEntry);

    // track selection
    auto goodTrack = [&](auto trdQC) {
      goodTracklets = 0x3F; // reset
      det.fill(-1);
      nTracklets = trdQC.trackTRD.getNtracklets();
      dEdxBefore->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
      if (nTracklets < 4) {
        return false;
      }
      if (trdQC.trackTRD.getChi2() < 6) {
        return false;
      }
      if (trdQC.trackTRD.getHasPadrowCrossing()) {
        return false;
      }
      if (trdQC.trackSeed.getP() < 0.48) {
        return false;
      }
      if (trdQC.trackSeed.getP() > P_MAX) {
        return false;
      }
      for (auto i = 0; i < 6; ++i) {
        auto trklt = trdQC.trklt64[i];
        if (abs(trdQC.trackletY[i]) < 10.0 || noiseMap.getIsNoisy(trklt.getHCID(), trklt.getROB(), trklt.getMCM()) || trdQC.trackTRD.getTrackletIndex(i) == -1) {
          goodTracklets &= ~(1 << i);
        }
        if (trklt.getQ2() >= 62 || trklt.getQ2() < 6) {
          goodTracklets &= ~(1 << i);
        }
        if (trklt.getQ1() >= 127 || trklt.getQ1() < 6) {
          goodTracklets &= ~(1 << i);
        }
        if (trklt.getQ0() >= 127 || trklt.getQ0() < 6) {
          goodTracklets &= ~(1 << i);
        }

        if (trdQC.trackTRD.getHasNeighbor()) { // only mostly good tracks should be plotted
          if (trdQC.trackTRD.getIsCrossingNeighbor(i)) {
            etaSplit[i]->Fill(trdQC.trackProp[i].getEta());
            goodTracklets &= ~(1 << i);
          }
        }
      }
      if (std::bitset<6>(goodTracklets).count() < 4) {
        return false;
      }
      for (auto i = 0; i < 6; ++i) {
        if (goodTracklets & (1 << i)) {
          det[i] = trdQC.trklt64[i].getDetector();
        }
      }
      return true;
    };

    // keep track of already matched ...-TOF tracks
    std::vector<bool> matchedTOF(qc.size(), false);

    auto tofSeparationEK = [&](double L, double p) {
      return L * std::abs(MassElectron * MassElectron - MassKaonCharged * MassKaonCharged) / (2 * p * p * 200.0);
    };
    auto tofSeparationEP = [&](double L, double p) {
      return L * std::abs(MassElectron * MassElectron - MassProton * MassProton) / (2 * p * p * 200.0);
    };
    auto combSepEK = [&](double L, double p) {
      return TMath::Sqrt(1. / 2. * (fEKSep->Eval(p) * fEKSep->Eval(p) + tofSeparationEK(L, p) * tofSeparationEK(L, p)));
    };
    auto combSepEP = [&](double L, double p) {
      return TMath::Sqrt(1. / 2. * (fEPSep->Eval(p) * fEPSep->Eval(p) + tofSeparationEP(L, p) * tofSeparationEP(L, p)));
    };

    // Fill out tree
    auto fillTree = [&](auto qc) {
      for (int i = 0; i < 6; ++i) {
        slope[i] = qc.trklt64[i].getSlopeBinSigned();
        pOut[i] = qc.trackProp[i].getP();
        etaOut[i] = qc.trackProp[i].getEta();
        phiOut[i] = qc.trackProp[i].getPhi();
        chargeUn[i * 3 + 0] = qc.trklt64[i].getQ0();
        chargeUn[i * 3 + 1] = qc.trklt64[i].getQ1();
        chargeUn[i * 3 + 2] = qc.trklt64[i].getQ2();
        charge[i * 3 + 0] = qc.trackletCorCharges[i][0];
        charge[i * 3 + 1] = qc.trackletCorCharges[i][1];
        charge[i * 3 + 2] = qc.trackletCorCharges[i][2];
      }
      sign = qc.trackProp[0].getSign();
      dEdxTPC = qc.dEdxTotTPC;
      outTree->Fill();
    };

    // perform backwards matching of tof tracks to (ITS-)TPC-TRD tracks
    auto tofMatching = [&](auto tofInfo) {
      // Indexing into qc gives the corresponding parent track for TOF tracks
      auto refIndex = tofInfo.getTrackRef().getIndex();
      auto trdQC = qc[refIndex];
      auto goodBit = highBit(goodTracklets);
      if (!goodTrack(trdQC)) { // track quality
        return;
      }
      auto L = tofInfo.getLTIntegralOut().getL(); // get track length (cm)
      auto time = tofInfo.getSignal();            // track time (ps)
      uint32_t bc = uint32_t((time - 10000) * o2::tof::Geo::BC_TIME_INPS_INV);
      time -= bc * o2::tof::Geo::BC_TIME_INPS;
      double p = trdQC.trackProp[goodBit].getSign() * trdQC.trackProp[goodBit].getP();
      double beta = L / time / kCSPEED;
      tof->Fill(p, beta);
      matchedTOF[refIndex] = true;
      double m2 = p * p / (L * L) * (kCSPEED * time - L) *
                  (kCSPEED * time + L); // mass2
      if (m2 < 0) {
        return;
      }
      double m = TMath::Sqrt(m2);
      tofMass = m;
      double g2 = p * p / m2 + 1.f; // gamma2
      double md = m * TMath::Sqrt(TMath::Power(0.001 / p, 2) +
                                  g2 * g2 *
                                    (TMath::Power(200.0 / time, 2) +
                                     TMath::Power(0.001 / L, 2)));

      // calculate mass hypo
      auto sigmaMassElectron = TMath::Abs(m - MassElectron) / md;
      auto sigmaMassKaon = TMath::Abs(m - MassKaonCharged) / md;
      auto sigmaMassProton = TMath::Abs(m - MassProton) / md;
      float elecSep = (trdQC.dEdxTotTPC - fElectron->Eval(trdQC.trackSeed.getP())) / (trdQC.dEdxTotTPC * dEdxResolution);
      float pionSep = (trdQC.dEdxTotTPC - fPion->Eval(trdQC.trackSeed.getP())) / (trdQC.dEdxTotTPC * dEdxResolution);
      if (sigmaMassElectron < 1.0 && sigmaMassKaon > 3.0 &&
          sigmaMassProton > 3.0 &&
          elecSep < 1. && elecSep >= 0.) {
        pid = 1;
        tofElectrons->Fill(p, beta);
        dEdxElectronsTOF->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
        dEdxAfter->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
      } else if (pionSep >= -1. && pionSep < 0.) {
        pid = 2;
        dEdxPionsTPC->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
        dEdxAfter->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
      } else {
        pid = 0;
        tofRest->Fill(p, beta);
        dEdxRestTOF->Fill(trdQC.trackSeed.getP(), trdQC.dEdxTotTPC);
      }
      tofTPC->Fill(beta, trdQC.dEdxTotTPC);
      fillTree(trdQC);
    };

    // TPC-TRD-TOF
    for (const auto& tof : TPCTRDTOFMatchInfo) {
      tofMatching(tof);
    }

    // ITS-TPC-TRD-TOF
    for (const auto& tof : ITSTPCTRDTOFMatchInfo) {
      tofMatching(tof);
    }

    for (size_t i = 0; i < qc.size(); ++i) {
      if (matchedTOF[i]) {
        continue;
      }

      const auto& q = qc[i];
      if (!goodTrack(q)) {
        continue;
      }

      float p = q.trackSeed.getSign() * q.trackSeed.getP();
      dEdx->Fill(p, q.dEdxTotTPC);

      float elecSep = (q.dEdxTotTPC - fElectron->Eval(q.trackSeed.getP())) / (q.dEdxTotTPC * dEdxResolution);
      float pionSep = (q.dEdxTotTPC - fPion->Eval(q.trackSeed.getP())) / (q.dEdxTotTPC * dEdxResolution);

      if ((isTPCCut(q.trackSeed.getP(), q.dEdxTotTPC) || fCombSep->Eval(q.trackSeed.getP()) >= sigma) && (elecSep < 1. && elecSep >= 0.) && q.dEdxTotTPC > 74 && q.dEdxTotTPC < 95) {
        pid = 1;
        dEdxElectronsTPC->Fill(p, q.dEdxTotTPC);
        dEdxAfter->Fill(q.trackSeed.getP(), q.dEdxTotTPC);
      } else if (pionSep > -1. && pionSep < 0.) {
        pid = 2;
        dEdxPionsTPC->Fill(p, q.dEdxTotTPC);
        dEdxAfter->Fill(q.trackSeed.getP(), q.dEdxTotTPC);
      } else {
        pid = 0;
        dEdxRestTPC->Fill(p, q.dEdxTotTPC);
      }
      fillTree(q);
    }

    printProgress(start, nEntries, (double)iEntry / (double)nEntries);
  }
  outTree->Write();
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration =
    std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  printf("\n\n\n\n\nDone in %ld (s)!\n", duration.count());

  // ----------------------------------------------------
  // Draw
  // ----------------------------------------------------
  std::unique_ptr<TFile> outFileQCPtr(
    TFile::Open("particleSkimmerQC.root", "RECREATE"));
  outFileQCPtr->cd();
  auto c = new TCanvas("dEdxCanvas", "dEdxTPC Canvas");
  c->Divide(2, 1);
  c->cd(1);
  dEdx->Draw("colz");
  c->cd(2);
  gPad->Divide(2, 2);
  gPad->cd(1);
  dEdxElectronsTPC->Draw("colz");
  c->cd(2);
  gPad->cd(2);
  dEdxPionsTPC->Draw("colz");
  c->cd(2);
  gPad->cd(3);
  dEdxElectronsTOF->Draw("colz");
  c->cd(2);
  gPad->cd(4);
  dEdxRestTOF->Draw("colz");
  c->Write();

  c = new TCanvas("tofCanvas", "tofBetaCanvas");
  c->Divide(2, 1);
  c->cd(1);
  tof->Draw("colz");
  c->cd(2);
  gPad->Divide(1, 2);
  gPad->cd(1);
  tofElectrons->Draw("colz");
  c->cd(2);
  gPad->cd(2);
  tofRest->Draw("colz");
  c->Write();

  c = new TCanvas("tofTPC", "tofTPC");
  c->cd();
  tofTPC->Draw("colz");
  c->Write();

  c = new TCanvas("cEtaSplit", "Eta Split");
  c->cd();
  etaSplit[0]->Draw();
  etaSplit[1]->Draw("same");
  etaSplit[2]->Draw("same");
  etaSplit[3]->Draw("same");
  etaSplit[4]->Draw("same");
  etaSplit[5]->Draw("same");
  c->BuildLegend();
  c->Write();

  c = new TCanvas("cCuts", "Particle Cuts");
  c->Divide(2, 1);
  auto cp = c->cd(1);
  dEdxBefore->Draw("colz");
  fPion->Draw("SAME");
  fElectron->Draw("SAME");
  fMuon->Draw("SAME");
  fKaon->Draw("SAME");
  fProton->Draw("SAME");
  cp->SetLogx();
  cp->SetLogz();
  cp = c->cd(2);
  dEdxAfter->Draw("colz");
  fPion->Draw("SAME");
  fElectron->Draw("SAME");
  fMuon->Draw("SAME");
  fKaon->Draw("SAME");
  fProton->Draw("SAME");
  cp->SetLogx();
  cp->SetLogz();
  c->Write();
}
