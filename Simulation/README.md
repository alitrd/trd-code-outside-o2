# TRD Simulation Documentation

## Creating Pulse-Height Plots

```CreatePulseHeightPlot.C``` is a macro to create pulse-height plots using (real or simulated) TRD digits. As input files it takes:

- ```o2sim_geometry-aligned.root```
- ```o2sim_grp.root```
- ```trddigits.root```
- ```trdtracklets.root```
- ```o2_tfidinfo.root```

The macro uses the ```DataManager.C``` to read out the digit data, where it loops over all time frames and events. The digits are clustered into Continuous Regions, that are regions of digits in the same detector and pad row, and adjacent pad columns. From these Continuous Regions it finds Regions of Interest (RoI) that are said to constitute a signal. The Region of Interest contain the digit in the continuous region with the highest ADC sum value, its neighbouring pad with the highest ADC sum value, and the two pads adjacent to them. Quality cuts are applied to filter out low quality signals. From these Regions of Interest, the pulse-height plot is calculated, by adding and later averaging the ADC values in each time bin.
CreatePulseHeightPlot.C creates a file pulseheight.root that contains:

- ```pulse-height```, a 1-dim histogram containing the pulse-height plot
- ```tb_adc```, a 2-dim histogram showing the distribution of ADC values in each time bin
- ```ADC```, a directory that contains histograms showing the distribution of ADC values (such as the values themselves, the sum in each digit, etc.)
- ```Signals```, a directory containg 2-dim histograms of the signals extracted
- ```Signals_1d```, a directory containg the 1-dim projections of the histograms in Signals

As a further check, digits can be excluded when they are matched to (TPC or ITS-TPC) tracks that have pad row crossings. In this case, the filed ```trdmatches_itstpc.root``` and/or ```trdmatches_tpc.root``` must be provided, and instead of the ```DataManager.C``` macro, the ```DataManagerTracks.C``` macro is used. 
To create the input files, the O2 TRD [simulation](https://github.com/AliceO2Group/AliceO2/tree/dev/Detectors/TRD/simulation) and [reconstruction](https://github.com/AliceO2Group/AliceO2/tree/dev/Detectors/TRD/workflow) workflow is used. An examplary workflow could be for MC:

```bash
o2-sim -m PIPE MAG TRD -n 5000 -g boxgen --configKeyValues 'BoxGun.pdg=211;BoxGun.eta[0]=-0.84;BoxGun.eta[1]=0.84;BoxGun.prange[0]=1.;BoxGun.prange[1]=5.'
o2-sim-digitizer-workflow --configKeyValues 'TRDSimParams.trf=1'
o2-tpc-reco-workflow --input-type digits --output-type clusters,tracks 
o2-its-reco-workflow --trackerCA --tracking-mode async
o2-tpcits-match-workflow --tpc-track-reader tpctracks.root --tpc-native-cluster-reader "--infile tpc-native-clusters.root"
o2-trd-tracklet-transformer --filter-trigrec
o2-trd-global-tracking --filter-trigrec
```

where only the first two tasks are needed if no matching to ITS/ITS-TPC is done. The ```TRDSimParams.trf``` configurable in ```o2-sim-digitizer-workflow``` allows the user to chose different Time-Response Functions to digitize the hits. It can either use the default O2 and AliRoot TRF ```trf=0```, the TRF presented in the TRD TDR ```trf=1```, no TRF ```trf=2``` or a Landau distribution as TRF ```trf=3```, where the parameters ```TRDSimParams.trf_landau_mu``` and ```TRDSimParams.trf_landau_sigma``` can be set by the user.

## Time-Response Function Convolutions

```TRFConvolutions.py``` is a short Python programme to show the influence of different TRFs on the shape of the pulse-height plots. It contains different pulse-height plots (one from real data from run 526689, and three from MC simulations using no TRF, the default O2 TRF, and the TRD TDR TRF) and various TRFs. By using the method ```convolve_php```, the pulse-height plot with no TRF can be convolved with the TRFs to show their influence on the pulse-height plot. Since the time binning has to be equal for the pulse-height plot and the TRF that are convolved, it can be chosen which of their binnings is to be used.
Autocorrelations of the pulse-height plots can be extracted using the ```autocorr``` method.
